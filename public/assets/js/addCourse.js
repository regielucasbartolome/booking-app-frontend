let courseForm = document.querySelector("#createCourse");

courseForm.addEventListener("submit", (e) => {
	e.preventDefault();

	let courseName = document.querySelector("#courseName").value;
	//=> .value => describes the value "attribute" of the HTML element.
	//console.log(courseName)
	let coursePrice = document.querySelector("#coursePrice").value;
	//console.log(coursePrice)
	let courseDescription = document.querySelector("#courseDescription").value;
	//console.log(courseDescription)
	
	if(courseName !== "" && coursePrice !== "" && courseDescription !== "") {
		fetch("https://aqueous-sierra-72637.herokuapp.com/api/courses/course-exist", {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				name: courseName
			})
		}).then(res => res.json()
		).then(data => {
			if(data === false) {
				fetch("https://aqueous-sierra-72637.herokuapp.com/api/courses/addCourse", {
					method: 'POST',
					headers: {
						'Content-Type':'application/json'
					},
					body: JSON.stringify({
						    name: courseName,
						    description: courseDescription,
						    price: coursePrice
					})
					//after describing the structure of the request body, now create the structure of the response coming from the back end.
				}).then(res => {
						return res.json()
				}).then(data => {
					console.log(data);

					if(data === true) {
						alert(`New course is added successfully.`);
						courseForm.reset();
					}else {
						alert(`Something went wrong in adding the course.`);
					}
				})
			}else {
				alert(`Course already exist. Create new course instead.`)
			}
		})
	}else {
		alert(`All fields are requiered`);
	}
	//save the new entry inside the database by describing teh request method/structure
	/*fetch("http://localhost:4000/api/courses/addCourse", {
		method: 'POST',
		headers: {
			'Content-Type':'application/json'
		},
		body: JSON.stringify({
			    name: courseName,
			    description: courseDescription,
			    price: coursePrice
		})
		//after describing the structure of the request body, now create the structure of the response coming from the back end.
	}).then(res => {
			return res.json()
	}).then(data => {
		console.log(data);

		if(data === true) {
			alert(`New course is added successfully.`);
		}else {
			alert(`Something went wrong in adding new course`);
		}
	})*/
})
